(function($){
 
	$.fn.extend({
		puzzle: function(attr){
			var $current_piece		= '';
			var $current_target 	= '';
			var $moves 				= 0;

			defaults = {    		
	            bW: 	4,																			//number of pieces per rows
	            bH: 	4,																			//number of pieces per cols
	            bI: 	'puzzle1',																	//Puzzle image (see style.css)
	            Ms: 	'Order the pieces in the right way',										//Puzzle message
	            Em:     'Congratulations!',															//Solved puzzle message
	            Lb:     ['Correct','Wrong','Total','Movements'],
	            BC: 	'#09190f', 
			}
			var opt 	= $.extend({}, defaults, attr);
			var endCl 	= convertHex(opt.BC,80);

			/* creating board */
	        var cont    = 1;
	        var item    = '<li class="target" id="target_{cont}"><div class="piece" id="piece_{cont}"></div></li>';
	        var html    = ''; 
	        for($x=1;$x<=opt.bW;$x++){
	            for($y=1;$y<=opt.bH;$y++){
	                html+=item.replace(/{cont}/g,cont);
	                cont++;
	            }
	        }

	        this.addClass(opt.bI).css("background-color",opt.BC);
	        this.append('<div class="col_01"><h1>'+opt.Ms+'</h1><div class="solved"></div></div>'); //left column (text al solved puzzle)
	        this.append('<div class="col_02"><ul id="board">'+html+'</ul></div>');					//right column puzzle board
	        this.append('<div id="popup"><h2>'+opt.Em+'</h2><a href="#">X</a></div>');				//popup end message
	        this.find('.col_01').append('<div class="scoreboard"></div>');							//Add scoreboard to the left column
	        this.find('#popup').css("background-color",endCl);
	        /* END board */

	        this.target     = $(".target");															//Areas where the pieces can be released
	        this.piece      = $('.piece');															//Each game piece
	        this.board      = $('ul#board');														//Game board
	        this.close      = $('#popup a');														//Popup close buttom

	        /* resizing pieces */
	        var wp = this.board.width()/opt.bW;             										//pieces width
	        var hp = this.height()/opt.bH;  														//pieces height
	        $('li.target, div.piece').width(wp).height(hp);
	        /* END resizing */

	        /* creating and unsort pieces */
	        var lista       = range(1,(opt.bW*opt.bH));												//create array with piece positions
	        var unsort      = lista.sort(function() {return Math.random() - 0.5});					//unsort array positions					
	        var xpos        = '';
	        var ypos        = '';
	        var style_tpl   = '.col_02 ul#board div[data_part="{posi}"]{background-position:{vals}}';
	        var style       = '';
	        cont = 0;
	        this.piece.each(function(){																//calculating the background position of each piece
	            xpos        = (cont%opt.bW) * wp * (-1);
	            ypos        = Math.floor(cont/opt.bW) * hp * (-1);
	            $(this).attr('data_part',unsort[cont]);												//asigning random position to each piece
	            style+=style_tpl.replace("{posi}",cont+1);
	            style = style.replace("{vals}",xpos+"px "+ypos+"px");
	            cont++;
	        });
	        $(this).after( "<style>"+style+"</style>" );											//creating style for each piece onfly
	        check_order();
	        /* END pieces */

	        /* drag and drop items */
	        this.piece.draggable({
	            containment:    "ul#board",
	            cursor:         "move",
	            start: function( event, ui ) {
	                $current_piece = $("#"+this.id);
	            }
	        }); 

	        this.target.droppable({
	            accept:             ".piece",
	            activeClass:        "highlight",
	            addClasses:         false,
	            drop: function( event, ui ) {
	                $current_target = $("#"+this.id);
	                change_pieces();
	            }
	        });
	        /* END drag and drop items */

	        this.close.click(function(){ 
	        	$('#popup').fadeOut(500);
	        	return false;
	        });

		    /* change pieces position when drag ends */
		    function change_pieces(){
		    	$current_piece.css('left',0).css('top',0);
		    	if( !$current_target.find('div').hasClass('correct') ){
			    	$cp_pos 		= $current_piece.attr('data_part');
			    	$ct_pos 		= $current_target.find('div').attr('data_part');
			    	$current_piece.attr('data_part',$ct_pos);
			    	$current_target.find('div').attr('data_part',$cp_pos);
			    	$moves++;
			    	check_order();
		    	} 
		    }
		    /* END change pieces */

		    /* check pieces order */
		    function check_order(){
		    	var error = false;
		    	var cont = 1;
		    	var wrong = 0;
		    	$('.piece').each(function(){
		    		if(cont != $(this).attr('data_part')){
		    			error = true;
		    			wrong++;
		    		} else {
		    			//Update pieces to prevent movement
		    			$(this).addClass('correct');
		    			$(this).removeClass('ui-draggable-dragging').removeClass('ui-draggable').removeClass('ui-draggable-handle');
		    			$(this).parent().html($(this).parent().html());
		    		}
		    		cont++;
		    	});
		    	if(!error){
		    		$('#popup').fadeIn(500);
		    	} 
	    		total 	= cont-1;
	    		text 	= opt.Lb[0]+": "+(total - wrong)+" ";
	    		text   += opt.Lb[1]+": "+wrong+"<br>";
	    		text   += opt.Lb[2]+": "+total+"<br>";
	    		text   += opt.Lb[3]+": "+$moves;
	    		$('.scoreboard').html(text);
		    }
		    /* END check order */

		    /* create pieces array */
		    function range(start, count) {
		        if(arguments.length == 1) {
		            count = start;
		            start = 0;
		        }
		        var foo = [];
		        for (var i = 0; i < count; i++) {
		            foo.push(start + i);
		        }
		        return foo;
		    }
		    /* END array */

			function convertHex(hex,opacity){
				hex = hex.replace('#','');
				r = parseInt(hex.substring(0,2), 16);
				g = parseInt(hex.substring(2,4), 16);
				b = parseInt(hex.substring(4,6), 16);

				result = 'rgba('+r+','+g+','+b+','+opacity/100+')';
				return result;
			}
		}
	});
 
})(jQuery)