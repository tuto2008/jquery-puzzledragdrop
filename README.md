# README #

Este plugin de jquery sirve para generar un juego de puzzle drag and drop muy sencillo. Pemite configurar en número de fichas y la imagen que se va a usar en el puzzle.

### Pasos para su implementación ###

* Header de la página
* Body
* Configuración
* Demo On-Line

### Header de la página ###

En el header de su html deberán estar los siguientes enlaces a la hoja de estilo y a los distintos js que se requieren:


```
#!javascript

<link rel="stylesheet" href="assets/style.css">
<script src="assets/jquery-1.10.2.js"></script>
<script src="assets/jquery-ui.js"></script>
<script src="assets/jquery.PuzzleDragDrop.js"></script>
<script src="assets/main.js"></script>
```


### Body ###


```
#!html

<body>
	<div id="container">
		<div id="game_area"></div>
	</div>
</body>
```


### Configuración ###

Para configurar el puzzle, abriremos el archivo main.js y podremos modificar el número de fichas por fila y por columna, la imagen que usaremos para el puzzle y el color de fondo del puzzle.


```
#!javascript

$(function() {
	$('#game_area').puzzle({
        bW:     3,
        bH:     3,
        bI:     'puzzle4',
        BC: 	'#440f38',
    });
});
```

### DEMO ON-LINE ###

Puedes visitar una demo on-line del plugin en esta ruta: [DEMO](http://demos.luismarcano.com/PuzzleDragDrop).